FROM        debian:buster-slim
MAINTAINER  Michael Friedrich <mfriedrich@gitlab.com>

COPY prometheus_demo_services  /bin/prometheus_demo_services

EXPOSE     8080
ENTRYPOINT [ "/bin/prometheus_demo_services" ]
